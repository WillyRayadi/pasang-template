<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    public function index() {
        $category=Category::all();
        return view('category', ['data'=>$category]);
    }

    public function edit($id){
        $idcategory=Category::where('id', $id)->first();
        return view('categoryedit', ['data'=>$idcategory]);
    }

    public function update(Request $request, $id){
        $idcategory=Category::where('id', $id)->first();
        $idcategory->category_name = $request->category;
        $idcategory->save();

        $category=Category::all();
        return view('category', ['data'=>$category]);
    }

    public function delete($id){
        $idcategory=Category::where('id', $id)->first();
        $idcategory->destroy();

        $category=Category::all();
        return view('category', ['data'=>$category]);
    }
}
// all
// get
// first