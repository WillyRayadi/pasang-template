<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subcategory;

class SubcategoryController extends Controller
{
    public function index() {
        $subcategory=Subcategory::all();
        return view('subcategory', ['data'=>$subcategory]);
    }
}
