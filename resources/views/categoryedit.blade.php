@extends("layouts.master")

@section("content")
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Category</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Category</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">

    <div class="container-fluid">
    <form method="POST" action="{{url('/')}}/product/edit/{{$data->id}}">
    @csrf
    <div class="form-group">
        <label for="exampleInputEmail1">Category</label>
        <input type="text" name="category" value="{{$data->category_name}}" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukan Category">
        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
    </form>

    </div>
        <!-- /.row (main row) -->
    <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection
