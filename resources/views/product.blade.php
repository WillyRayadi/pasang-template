@extends("layouts.master")

@section("content")
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Product</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Product</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">

    <div class="container-fluid">
    <table class="table">
    <thead>
        <tr>
        <th scope="col">No</th>
        <th scope="col">Category ID</th>
        <th scope="col">Subcategory ID</th>
        <th scope="col">Product Name</th>
        <th scope="col">Price</th>
        <th scope="col">Stock</th>
        <th scope="col">Created At</th>
        <th scope="col">Updated At</th>
        </tr>
    </thead>
    <tbody>
    @foreach($data as $value)
        <tr>
        <th scope="row">{{$value->id}}</th>
        <td>{{$value->category->category_name}}</td>
        <td>{{$value->subcategory->subcategory_name}}</td>
        <td>{{$value->product_name}}</td>
        <td>{{$value->price}}</td>
        <td>{{$value->stock}}</td>
        <td>{{$value->created_at}}</td>
        <td>{{$value->updated_at}}</td>
        </tr>
    @endforeach
    </tbody>
    </table>
    </div>
        <!-- /.row (main row) -->
    <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection
